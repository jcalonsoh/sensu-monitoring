#!/usr/bin/env ruby
require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'

class DiskMetricIOBandwidth < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}.disk"

  option :convert,
         :description => 'Convert devicemapper to logical volume name',
         :short => '-c',
         :long => '--convert',
         :default => false

  option :warn,
         :short => '-w WARN',
         :proc => proc {|a| a.to_f},
         :default => 70

  option :crit,
         :short => '-c CRIT',
         :proc => proc {|a| a.to_f},
         :default => 90

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def chop(string)
    string.to_s.split
  end

  def run_exec
    begin
      `iostat -dx | awk 'NR>=4'` if `iostat -dx`.exist?
    rescue => error
      puts "Faild to: #{error}"
    end
  end

  def run
    command = (config[:mocking].nil?) ? run_exec : config[:mocking]
    if command.nil? === false
      metrics = {}
      command.each_line do |line|
        result = chop(line)
        metric = {
            :rrqms        => result[1].to_f,
            :wrqms        => result[2].to_f,
            :rs           => result[3].to_f,
            :ws           => result[4].to_f,
            :rkBs         => result[5].to_f,
            :wkBs         => result[6].to_f,
            :avgrq_sz     => result[7].to_f,
            :avgqu_sz     => result[8].to_f,
            :await        => result[9].to_f,
            :r_await      => result[10].to_f,
            :w_await      => result[11].to_f,
            :svctm        => result[12].to_f,
            :util         => result[13].to_f
        }
        metrics[result[0]] = metric
      end

      metrics.each do |parent, children|
        children.each do |child, value|
          output [config[:scheme], parent, child].join('.'), value
        end

        children.each do |child, value|
          if child.match(/util/)
            warning if value.between?(config[:warn], config[:crit] - 1)
            critical if value >= config[:crit]
          end
        end

      end
    end

    ok
  end

end
