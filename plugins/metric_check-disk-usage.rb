#!/usr/bin/env ruby
# came from https://github.com/sensu/sensu-community-plugins/blob/master/plugins/system/disk-capacity-metrics.rb

require 'sensu-plugin/metric/cli'
require 'socket'

class DiskCapacityMetric < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to .$parent.$child",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}.disk"

  option :warn,
         :short => '-w WARN',
         :proc => proc {|a| a.to_f },
         :default => 80

  option :crit,
         :short => '-c CRIT',
         :proc => proc {|a| a.to_f },
         :default => 90

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-mpi',
         :long => '--mockingpi'

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-mpt',
         :long => '--mockingpt'

  def run_exec(string)
    begin
      string.split("\n").drop(1) if string.nil? == false
    rescue => error
      puts "Faild to: #{error}"
    end
  end

  def run_mocking(string)
    begin
      string.split("\n").drop(1)
    rescue => error
      puts "Faild to: #{error}"
    end
  end

  def run
    commandpt = (config[:mockingpt].nil?) ? run_exec(`df -PT`) : run_mocking(config[:mockingpt])

    if commandpt.nil? === false
      commandpt.each do |line|
        fs, _type, blocks, used, avail, capacity, _mnt = line.split

        if fs.match('/dev')
          fs = fs.gsub('/dev/', '')
          metrics = {
              :disk => {
                  "#{fs}.size" => blocks,
                  "#{fs}.used" => used,
                  "#{fs}.avail" => avail,
                  "#{fs}.capacity" => capacity.gsub('%', '')
              }
          }
          metrics.each do |parent, children|
            children.each do |child, value|
              output [config[:scheme], parent, child].join("."), value

              if child.match(/capacity/)
                if value.to_f.between?(config[:warn],config[:crit]-1)
                  warning
                elsif value.to_f >= config[:crit]
                  critical
                end
              end
            end
          end
        end
      end
    end

    # Get inode capacity metrics
    commandpi = (config[:mockingpi].nil?) ? run_exec(`df -Pi`) : run_mocking(config[:mockingpi])

    if commandpt.nil? === false
      commandpi.each do |line|
        fs, inodes, used, avail, capacity, _mnt = line.split

        if fs.match('/dev')
          fs = fs.gsub('/dev/', '')
          metrics = {
              :disk=> {
                  "#{fs}.inodes" => inodes,
                  "#{fs}.iused" => used,
                  "#{fs}.iavail" => avail,
                  "#{fs}.icapacity" => capacity.gsub('%', '')
              }
          }
          metrics.each do |parent, children|
            children.each do |child, value|
              output [config[:scheme], parent, child].join("."), value

              if child.match(/icapacity/)
                if value.to_f.between?(config[:warn],config[:crit]-1)
                  warning
                elsif value.to_f >= config[:crit]
                  critical
                end
              end
            end
          end
          end
      end
    end
    ok
  end
end
