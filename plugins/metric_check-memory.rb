#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'
require_relative '../lib/filereader'

class MemoryMetricCheck < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}.memory"

  option :warn,
         :short => '-w WARN',
         :proc => proc {|a| a.to_f },
         :default => 70

  option :crit,
         :short => '-c CRIT',
         :proc => proc {|a| a.to_f },
         :default => 90

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def run
    # Metrics borrowed from hoardd: https://github.com/coredump/hoardd
    command = (config[:mocking].nil?) ? filereader(`/proc/meminfo`) : config[:mocking]

    if command.to_s != ''

      mem = {}
      command.each_line do |line|
        mem['total']     = line.split(/\s+/)[1].to_i * 1024 if line.match(/^MemTotal/)
        mem['free']      = line.split(/\s+/)[1].to_i * 1024 if line.match(/^MemFree/)
        mem['buffers']   = line.split(/\s+/)[1].to_i * 1024 if line.match(/^Buffers/)
        mem['cached']    = line.split(/\s+/)[1].to_i * 1024 if line.match(/^Cached/)
        mem['swapTotal'] = line.split(/\s+/)[1].to_i * 1024 if line.match(/^SwapTotal/)
        mem['swapFree']  = line.split(/\s+/)[1].to_i * 1024 if line.match(/^SwapFree/)
      end

      mem['swapUsed'] = mem['swapTotal'] - mem['swapFree']
      mem['used'] = mem['total'] - mem['free']
      mem['usedWOBuffersCaches'] = mem['used'] - (mem['buffers'] + mem['cached'])
      mem['freeWOBuffersCaches'] = mem['free'] + (mem['buffers'] + mem['cached'])

      memory_usage = ((mem['free'].to_f / mem['total'].to_f) * 100.0).to_i

      mem.each do |k, v|
        output [config[:scheme], 'memory', k].join("."), v
      end

      if memory_usage.between?(config[:warn], config[:crit] - 1)
        warning
      elsif memory_usage >= config[:crit]
        critical
      else
        ok
      end
    end

    ok

  end

end
