#!/usr/bin/env ruby
require File.expand_path('./lib/filereader.rb')
require 'sensu-plugin/metric/cli'
require 'socket'

class DiskMetricIO < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metric",
         :short => "-s SCHEME",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}.disk"

  option :convert,
         :description => 'Convert devicemapper to logical volume name',
         :short => '-c',
         :long => '--convert',
         :default => false

  option :warnr,
         :short => '-wr WARNR',
         :proc => proc {|a| a.to_f},
         :default => 2000

  option :warnw,
         :short => '-ww WARNW',
         :proc => proc {|a| a.to_f},
         :default => 600

  option :critr,
         :short => '-cr CRITR',
         :proc => proc {|a| a.to_f},
         :default => 3000

  option :critw,
         :short => '-cw CRITW',
         :proc => proc {|a| a.to_f},
         :default => 800

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def run
    # http://www.kernel.org/doc/Documentation/iostats.txt
    command = (config[:mocking].nil?) ? readfile('/proc/diskstats') : config[:mocking]

    metrics = [
      'reads', 'readsMerged', 'sectorsRead', 'readTime',
      'writes', 'writesMerged', 'sectorsWritten', 'writeTime',
      'ioInProgress', 'ioTime', 'ioTimeWeighted'
    ]

    command.each_line do |line|
      stats = line.strip.split(/\s+/)
      _major, _minor, dev = stats.shift(3)
      if config[:convert]
        if dev =~ /^dm-.*$/
          dev = `lsblk -P -o NAME /dev/"#{dev}"| cut -d\\" -f2`.lines.first.chomp!
        end
      end
      next if stats == ['0'].cycle.take(stats.size)

      metrics.size.times { |i|
        output "#{config[:scheme]}.#{dev}.#{metrics[i]}", stats[i]

        if metrics[i].match(/reads/)
          if stats[i].to_f.between?(config[:warnr],config[:critr]-1)
            warning
          elsif stats[i].to_f >= config[:critr]
            critical
          end
        elsif metrics[i].match(/writes/)
          if stats[i].to_f.between?(config[:warnw],config[:critw]-1)
            warning
          elsif stats[i].to_f >= config[:critw]
            critical
          end
        end

      }
    end

    ok
  end

end
