#!/usr/bin/env ruby

require 'sensu-plugin/metric/cli'
require 'socket'

class MPStatMetricCheck < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to .$parent.$child",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}.mpstat"

  option :warn,
         :short => '-w WARN',
         :proc => proc {|a| a.to_f },
         :default => 70

  option :crit,
         :short => '-c CRIT',
         :proc => proc {|a| a.to_f },
         :default => 90

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def chop(string)
    string.to_s.split.map(&:to_f)
    # 15:34:29 up 4 days, 23:35,  1 user,  load average: 90.20, 80.27, 70.22
  end

  def run_exec
    begin
      `mpstat | tail -n1` if `mpstat`.exist?
    rescue => error
      puts "Faild to: #{error}"
    end
  end

  def run
    command = (config[:mocking].nil?) ? run_exec : config[:mocking]

    if command.nil? === false
      result = chop(command)
      metrics = {
          :cpu => {
              :user => result[3],
              :nice => result[4],
              :system => result[5],
              :iowait => result[6],
              :irq => result[7],
              :soft => result[8],
              :steal  => result[9],
              :guest => result[10],
              :idle => result[11]
          }
      }
      metrics.each do |parent, children|
        children.each do |child, value|
          output [config[:scheme], parent, child].join('.'), value
        end
      end

      checked_usage = 100 - metrics[:cpu][:idle].to_i

      if checked_usage.between?(config[:warn], config[:crit] - 1)
        warning
      elsif checked_usage >= config[:crit]
        critical
      else
        ok
      end
    end
    ok
  end

end
