#!/usr/bin/env ruby
# Basically copied from sensu-community-plugins/plugins/system/vmstat-metrics.rb

require 'sensu-plugin/metric/cli'
require 'socket'

class LoadStatMetricCheck < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => "Metric naming scheme, text to prepend to .$parent.$child",
         :long => "--scheme SCHEME",
         :default => "#{Socket.gethostname}"

  option :'5warn',
         :short => '-w5 WARN',
         :proc => proc {|a| a.to_f},
         :default => 70

  option :'10warn',
         :short => '-w10 WARN',
         :proc => proc {|a| a.to_f},
         :default => 60

  option :'15warn',
         :short => '-w15 WARN',
         :proc => proc {|a| a.to_f},
         :default => 60

  option :'5crit',
         :short => '-c5 WARN',
         :proc => proc {|a| a.to_f},
         :default => 90

  option :'10crit',
         :short => '-c10 WARN',
         :proc => proc {|a| a.to_f},
         :default => 80

  option :'15crit',
         :short => '-c15 WARN',
         :proc => proc {|a| a.to_f},
         :default => 70

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def chop(string)
    string.to_s.gsub(',', '').split.map(&:to_f)[-3..-1]
    # 15:34:29 up 4 days, 23:35,  1 user,  load average: 90.20, 80.27, 70.22
  end

  def run
    command = (config[:mocking].nil?) ? `uptime` : config[:mocking]
    result = chop(command)
    metrics = {
      :load_avg => {
        :'5' => result[0],
        :'10' => result[1],
        :'15' => result[2]
      }
    }

    metrics.each do |parent, children|
      children.each do |child, value|
        output [config[:scheme], parent, child].join('.'), value
      end
    end

    if metrics[:load_avg][:'5'].between?(config[:'5warn'],config[:'5crit']) || metrics[:load_avg][:'10'].between?(config[:'10warn'],config[:'10crit']) || metrics[:load_avg][:'15'].between?(config[:'15warn'],config[:'15crit'])
      warning
    elsif metrics[:load_avg][:'5'] >= config[:'5crit'] || metrics[:load_avg][:'10'].to_i >= config[:'10crit'] || metrics[:load_avg][:'15'] >= config[:'15crit']
      critical
    else
      ok
    end
  end

end
