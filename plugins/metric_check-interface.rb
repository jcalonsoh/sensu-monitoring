#!/usr/bin/env ruby

# Stats are using nicstat at http://nicstat.sourceforge.net/
# and doc at http://www.brendangregg.com/K9Toolkit/nicstat.c

require 'sensu-plugin/metric/cli'
require 'socket'
require 'json'

class InterfaceMetricCheck < Sensu::Plugin::Metric::CLI::Graphite

  option :scheme,
         :description => 'Metric naming scheme, text to prepend to metric',
         :short => '-s SCHEME',
         :long => '--scheme SCHEME',
         :default => "#{Socket.gethostname}.interface"

  option :interface,
         :description => 'Default interface upon check alert',
         :short => '-i INTERFACE',
         :long => '--interface INTERFACE',
         :default => 'eth0'

  option :warn,
         :short => '-w WARN',
         :proc => proc {|a| a.to_f },
         :default => 50

  option :crit,
         :short => '-c CRIT',
         :proc => proc {|a| a.to_f },
         :default => 70

  option :mocking,
         :description => 'This option is only for Testing',
         :short => '-m',
         :long => '--mocking'

  def chop(string)
    string.to_s.split
  end

  def run_exec
    begin
      `nicstat` if `nicstat`.exist?
    rescue => error
      puts "Faild to: #{error}"
    end
  end

  def run
    command = (config[:mocking].nil?) ? run_exec : config[:mocking]

    if command.nil? === false

      metrics = {}
      command.each_line do |line|
        result = chop(line)
        next if result[1].match(/Int/)
        metric = {
            :rKBs      => result[2].to_f,
            :wKBs      => result[3].to_f,
            :rPks      => result[4].to_f,
            :wPks      => result[5].to_f,
            :rAvs      => result[6].to_f,
            :wAvs      => result[7].to_f,
            :util      => result[8].to_f,
            :sat       => result[9].to_f
        }
        metrics[result[1]] = metric
      end

      metrics.each do |parent, children|
        children.each do |child, value|
          output [config[:scheme], parent, child].join('.'), value
        end

        if parent.match(config[:interface])
          children.each do |child, value|
            if child.match(/util/)
              warning if value.between?(config[:warn], config[:crit] - 1)
              critical if value >= config[:crit]
            elsif child.match(/sat/)
              critical if value >= 0.1
            end
          end
        end

      end
    end

    ok

  end

end
