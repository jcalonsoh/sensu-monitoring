#!/usr/bin/env ruby

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-handler'
require 'influxdb'
require 'timeout'
require 'json'


class InfluxHandler < Sensu::Handler
  def filter; end

  def event_name
    @event['client']['name'] + '/' + @event['check']['name'] + '/influxdb'
  end

  def handle
    influxdb = InfluxDB::Client.new settings['influx']['database'],
                                    host: settings['influx']['host'],
                                    port: settings['influx']['port'],
                                    username: settings['influx']['user'],
                                    password: settings['influx']['password']

    begin
      host = @event['client']['name']
      series = @event['check']['name']
      output = @event['check']['output']
      points = Array.new
      output.split(/\n/).each do |line|
        k,v,t = line.split(/\s+/)
        v = v.match('\.').nil? ? Integer(v) : Float(v) rescue v.to_s
        k.gsub!(/^.*#{settings['influx']['strip_metric']}\.(.*$)/, '\1') if settings['influx']['strip_metric']
        points << {:time => t.to_f, :metric => k, :value => v}
        influxdb.write_point(host + '.' + series, points)
      end
    end

    #puts "influxdb SERIES #{host + '.' + series}"
    #puts "influxdb POINTS #{JSON.pretty_generate(points)}"
    #puts "influxdb POINTS #{output}"
    #influxdb.write_point(series, points, true)

    rescue Timeout::Error
      puts "influxdb -- timed out while sending metrics"
    rescue => error
      puts "influxdb -- failed to send metrics : #{error}"
  end
end
