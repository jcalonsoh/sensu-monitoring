#!/usr/bin/env ruby

require 'sensu-handler'
require 'mongo'
require 'timeout'
require 'json'
require 'time'
require 'date'

include Mongo

class MongoHandler < Sensu::Handler
  def filter; end

  def event_name
    @event['client']['name'] + '/' + @event['check']['name'] + '/mongodb'
  end

  def handle
    mongodb = MongoClient.new(settings['mongodb']['host'], settings['mongodb']['port']).db(settings['mongodb']['database'])

    begin
      output = @event['check']['output']
      points = Array.new
      output.split(/\n/).each do |line|
        k,v,_t = line.split(/\s+/)
        v = v.match('\.').nil? ? Integer(v) : Float(v) rescue v.to_s
        points << {:metric => k, :value => v}
      end

      time = Time.now.utc.iso8601
      doc = {
          :timestamp     => time,
          :host          => @event['client']['name'],
          :address       => @event['client']['address'],
          :timestamp     => @event['check']['issued'],
          :check_name    => @event['check']['name'],
          :status        => @event['check']['status'],
          :metrics       => points
      }

      date = DateTime.parse(time).strftime("%Y%m%d")

      coll = mongodb.collection("#{@event['client']['name']}-#{date}")

      coll.insert(doc)

    end

  rescue Timeout::Error
    puts "mongodb -- timed out while sending metrics"
  rescue => error
    puts "mongodb -- failed to send metrics : #{error}"
  end
end
