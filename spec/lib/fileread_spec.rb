require_relative '../spec_helper'
require_relative '../../lib/filereader'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'
require 'minitest-spec-context'

describe '#FileRead' do

  it "It must not read file" do
    readfile('hola').must_be_nil
  end

end

