require_relative '../spec_helper'
require_relative '../../plugins/metric_check-disk-io'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'
require 'minitest-spec-context'

describe '#DiskMetricIO' do

  let(:diskmetricio) { DiskMetricIO.new }
  let(:contents_ok) { contents_file('proc-diskstats/ok') }
  let(:contents_warning_w) { contents_file('proc-diskstats/warning_w') }
  let(:contents_warning_r) { contents_file('proc-diskstats/warning_r') }
  let(:contents_critical_w) { contents_file('proc-diskstats/critical_w') }
  let(:contents_critical_r) { contents_file('proc-diskstats/critical_r') }

  before do
    diskmetricio.config[:warnr] = 250000
    diskmetricio.config[:warnw] = 60000
    diskmetricio.config[:critr] = 350000
    diskmetricio.config[:critw] = 80000
  end

  it "It's DiskIO Metric status OK" do
    begin
      diskmetricio.config[:mocking] = contents_ok
      diskmetricio.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  context "Alarming when its Reading" do
    it "It's DiskIO Metric status Warning Reading" do
      begin
        diskmetricio.config[:mocking] = contents_warning_r
        diskmetricio.run
      rescue SystemExit=>e
        e.status.must_equal(1)
      end
    end

    it "It's DiskIO Metric status Critical Reading" do
      begin
        diskmetricio.config[:mocking] = contents_critical_r
        diskmetricio.run
      rescue SystemExit=>e
        e.status.must_equal(2)
      end
    end
  end

  context "Alarming when its Writing" do
    it "It's DiskIO Metric status Warning Writing" do
      begin
        diskmetricio.config[:mocking] = contents_warning_w
        diskmetricio.run
      rescue SystemExit=>e
        e.status.must_equal(1)
      end
    end

    it "It's DiskIO Metric status Critical Writing" do
      begin
        diskmetricio.config[:mocking] = contents_critical_w
        diskmetricio.run
      rescue SystemExit=>e
        e.status.must_equal(2)
      end
    end
  end
end

