require_relative '../spec_helper'
require_relative '../../plugins/metric_check-disk-usage'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'
require 'minitest-spec-context'

describe '#DiskCapacityMetric' do

  let(:diskcapacity) { DiskCapacityMetric.new }
  let(:contents_okpi) { contents_file('dfdiskusage/ok_dfpi') }
  let(:contents_okpt) { contents_file('dfdiskusage/ok_dfpt') }
  let(:contents_warningpi) { contents_file('dfdiskusage/warning_dfpi') }
  let(:contents_warningpt) { contents_file('dfdiskusage/warning_dfpt') }
  let(:contents_criticalpi) { contents_file('dfdiskusage/critical_dfpi') }
  let(:contents_criticalpt) { contents_file('dfdiskusage/critical_dfpt') }

  before do
    diskcapacity.config[:warn] = 80
    diskcapacity.config[:crit] = 90
  end

  it "It's DiskUsageCapacity Metric status OK" do
    begin
      diskcapacity.config[:mockingpi] = contents_okpi
      diskcapacity.config[:mockingpt] = contents_okpt
      diskcapacity.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's DiskUsageCapacity Metric status WARNING" do
    begin
      diskcapacity.config[:mockingpi] = contents_warningpi
      diskcapacity.config[:mockingpt] = contents_warningpt
      diskcapacity.run
    rescue SystemExit=>e
      e.status.must_equal(1)
    end
  end

  it "It's DiskUsageCapacity Metric status CRITICAL" do
    begin
      diskcapacity.config[:mockingpi] = contents_criticalpi
      diskcapacity.config[:mockingpt] = contents_criticalpt
      diskcapacity.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end

  it "Just Testing run_exec" do
    diskcapacity.run_exec(`df Pi`).nil? == false
  end

  it "Just Testing run_mocking" do
    diskcapacity.run_exec(contents_okpi).nil? == false
  end

end

