require_relative '../spec_helper'
require_relative '../../plugins/metric_check-mpstat-cpu'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'

describe '#MPStatMetric' do

  let(:cpumetric) { MPStatMetricCheck.new }
  let(:contents_ok) { contents_file('proc-cpu/ok') }
  let(:contents_warning) { contents_file('proc-cpu/warning') }
  let(:contents_critical) { contents_file('proc-cpu/critical') }

  before do
    cpumetric.config[:warn] = 70
    cpumetric.config[:crit] = 90
  end

  it "It's CPU Metric status OK" do
    begin
      cpumetric.config[:mocking] = contents_ok
      cpumetric.run
    rescue SystemExit=>e
        e.status.must_equal(0)
    end
  end

  it "It's CPU Metric status Warning" do
    begin
      cpumetric.config[:mocking] = contents_warning
      cpumetric.run
    rescue SystemExit=>e
      e.status.must_equal(1)
    end
  end

  it "It's CPU Metric status Critical" do
    begin
      cpumetric.config[:mocking] = contents_critical
      cpumetric.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end
end