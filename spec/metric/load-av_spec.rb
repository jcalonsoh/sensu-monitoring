require_relative '../spec_helper'
require_relative '../../plugins/metric_check-loadaverage'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'

describe '#LoadStatMetric' do

  let(:loadav) { LoadStatMetricCheck.new }
  let(:contents_ok) { contents_file('load-av/ok') }
  let(:contents_warning) { contents_file('load-av/warning') }
  let(:contents_critical) { contents_file('load-av/critical') }

  before do
    loadav.config[:'5war'] = 70
    loadav.config[:'10war'] = 60
    loadav.config[:'15war'] = 60
    loadav.config[:'5crit'] = 90
    loadav.config[:'10crit'] = 80
    loadav.config[:'15crit'] = 70
  end

  it "It's Load Average Metric status OK" do
    begin
      loadav.config[:mocking] = contents_ok
      loadav.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's Load Average Metric status Warning" do
    begin
      loadav.config[:mocking] = contents_warning
      loadav.run
    rescue SystemExit=>e
      e.status.must_equal(1)
    end
  end

  it "It's Load Average Metric status Critical" do
    begin
      loadav.config[:mocking] = contents_critical
      loadav.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end
end