require_relative '../spec_helper'
require_relative '../../plugins/metric_check-interface'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'

describe '#InterfaceMetric' do

  let(:interface) { InterfaceMetricCheck.new }
  let(:contents_ok) { contents_file('interface/ok') }
  let(:contents_warning_usage) { contents_file('interface/warning_usage') }
  let(:contents_critical_usage) { contents_file('interface/critical_usage') }
  let(:contents_critical_sar) { contents_file('interface/critical_sar') }

  before do
    interface.config[:warn] = 50
    interface.config[:crit] = 70
    interface.config[:interface] = 'eth0'
  end

  it "It's Interface Metric status OK" do
    begin
      interface.config[:mocking] = contents_ok
      interface.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's Interface Metric status Critical by Usage" do
    begin
      interface.config[:mocking] = contents_critical_usage
      interface.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end

  it "It's Interface Metric status Critical by SAT" do
    begin
      interface.config[:mocking] = contents_critical_sar
      interface.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end

  it "It's Interface Metric status Warning by Usage" do
    begin
      interface.config[:mocking] = contents_warning_usage
      interface.run
    rescue SystemExit=>e
      e.status.must_equal(1)
    end
  end

end

