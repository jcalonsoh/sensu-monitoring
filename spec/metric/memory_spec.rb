require_relative '../spec_helper'
require_relative '../../plugins/metric_check-memory'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'

describe '#MemoryMetric' do

  let(:memory) { MemoryMetricCheck.new }
  let(:contents_ok) { contents_file('proc-memory/ok') }
  let(:contents_warning) { contents_file('proc-memory/warning') }
  let(:contents_critical) { contents_file('proc-memory/critical') }

  before do
    memory.config[:warn] = 70
    memory.config[:crit] = 60
  end

  it "It's Memory Metric status OK" do
    begin
      memory.config[:mocking] = contents_ok
      memory.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's Memory Metric status Warning" do
    begin
      memory.config[:mocking] = contents_warning
      memory.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's Memory Metric status Critical" do
    begin
      memory.config[:mocking] = contents_critical
      memory.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

end