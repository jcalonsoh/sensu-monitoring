require_relative '../spec_helper'
require_relative '../../plugins/metric_check-disk-bandwidth'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/unit'

describe '#DiskMetricIOBandwidth' do

  let(:diskiostat) { DiskMetricIOBandwidth.new }
  let(:contents_ok) { contents_file('diskstats-bandwith/ok') }
  let(:contents_warning) { contents_file('diskstats-bandwith/warning') }
  let(:contents_critical) { contents_file('diskstats-bandwith/critical') }

  before do
    diskiostat.config[:warn] = 70
    diskiostat.config[:crit] = 90
  end

  it "It's DiskMetricIOBandwidth Metric status OK" do
    begin
      diskiostat.config[:mocking] = contents_ok
      diskiostat.run
    rescue SystemExit=>e
      e.status.must_equal(0)
    end
  end

  it "It's DiskMetricIOBandwidth Metric status Warning" do
    begin
      diskiostat.config[:mocking] = contents_warning
      diskiostat.run
    rescue SystemExit=>e
      e.status.must_equal(1)
    end
  end

  it "It's DiskMetricIOBandwidth Metric status Critical" do
    begin
      diskiostat.config[:mocking] = contents_critical
      diskiostat.run
    rescue SystemExit=>e
      e.status.must_equal(2)
    end
  end
end