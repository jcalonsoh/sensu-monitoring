#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( sockstats => 1 );
sleep(1);
my $stat = $lxs->get;
my $sockstats  = $stat->sockstats;

print "used      $sockstats->{used}\n";
print "tcp       $sockstats->{tcp}\n";
print "udp       $sockstats->{udp}\n";
print "raw       $sockstats->{raw}\n";
print "ipfrag    $sockstats->{ipfrag}\n";

#    used    -  Total number of used sockets.
#    tcp     -  Number of tcp sockets in use.
#    udp     -  Number of udp sockets in use.
#    raw     -  Number of raw sockets in use.
#    ipfrag  -  Number of ip fragments in use (only available by kernels > 2.2).