#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;

my $lxs = Sys::Statistics::Linux->new( memstats => 1 );
sleep(1);
my $stat = $lxs->get;
my $memstats  = $stat->memstats;

print "memused      $memstats->{memused}\n";
print "memfree      $memstats->{memfree}\n";
print "memusedper   $memstats->{memusedper}\n";
print "buffers      $memstats->{buffers}\n";
print "cached       $memstats->{cached}\n";
print "realfree     $memstats->{realfree}\n";
print "realfreeper  $memstats->{realfreeper}\n";
print "swapused     $memstats->{swapused}\n";
print "swapfree     $memstats->{swapfree}\n";
print "swapusedper  $memstats->{swapusedper}\n";
print "swaptotal    $memstats->{swaptotal}\n";
print "swapcached   $memstats->{swapcached}\n";
print "active       $memstats->{active}\n";
print "inactive     $memstats->{inactive}\n";

#     memused         -  Total size of used memory in kilobytes.
#     memfree         -  Total size of free memory in kilobytes.
#     memusedper      -  Total size of used memory in percent.
#     memtotal        -  Total size of memory in kilobytes.
#     buffers         -  Total size of buffers used from memory in kilobytes.
#     cached          -  Total size of cached memory in kilobytes.
#     realfree        -  Total size of memory is real free (memfree + buffers + cached).
#     realfreeper     -  Total size of memory is real free in percent of total memory.
#     swapused        -  Total size of swap space is used is kilobytes.
#     swapfree        -  Total size of swap space is free in kilobytes.
#     swapusedper     -  Total size of swap space is used in percent.
#     swaptotal       -  Total size of swap space in kilobytes.
#     swapcached      -  Memory that once was swapped out, is swapped back in but still also is in the swapfile.
#     active          -  Memory that has been used more recently and usually not reclaimed unless absolutely necessary.
#     inactive        -  Memory which has been less recently used and is more eligible to be reclaimed for other purposes.
#                        On earlier kernels (2.4) Inact_dirty + Inact_laundry + Inact_clean.
#
#    The following statistics are only available by kernels from 2.6.
#    slab            -  Total size of memory in kilobytes that used by kernel for data structure allocations.
#    dirty           -  Total size of memory pages in kilobytes that waits to be written back to disk.
#    mapped          -  Total size of memory in kilbytes that is mapped by devices or libraries with mmap.
#    writeback       -  Total size of memory that was written back to disk.
#    committed_as    -  The amount of memory presently allocated on the system.
#
#    The following statistic is only available by kernels from 2.6.9.
#
#    commitlimit     -  Total amount of memory currently available to be allocated on the system.