#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( loadavg => 1 );
my $stat = $lxs->get;
my $loadavg  = $stat->loadavg;

print "avg_1        $loadavg->{avg_1}\n";
print "avg_5        $loadavg->{avg_5}\n";
print "avg_15       $loadavg->{avg_15}\n";

#    avg_1   -  The average processor workload of the last minute.
#    avg_5   -  The average processor workload of the last five minutes.
#    avg_15  -  The average processor workload of the last fifteen minutes.