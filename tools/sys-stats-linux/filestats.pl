#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( filestats => 1 );
my $stat = $lxs->get;
my $filestats  = $stat->filestats;

print "fhalloc        $filestats->{fhalloc}\n";
print "fhfree         $filestats->{fhfree}\n";
print "fhmax          $filestats->{fhmax}\n";
print "inalloc        $filestats->{inalloc}\n";
print "infree         $filestats->{infree}\n";
print "inmax          $filestats->{inmax}\n";
print "dentries       $filestats->{dentries}\n";
print "unused         $filestats->{unused}\n";
print "agelimit       $filestats->{agelimit}\n";
print "wantpages      $filestats->{wantpages}\n";

#    fhalloc    -  Number of allocated file handles.
#    fhfree     -  Number of free file handles.
#    fhmax      -  Number of maximum file handles.
#    inalloc    -  Number of allocated inodes.
#    infree     -  Number of free inodes.
#    inmax      -  Number of maximum inodes.
#    dentries   -  Dirty directory cache entries.
#    unused     -  Free diretory cache size.
#    agelimit   -  Time in seconds the dirty cache entries can be reclaimed.
#    wantpages  -  Pages that are requested by the system when memory is short.