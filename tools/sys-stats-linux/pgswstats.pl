#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( pgswstats => 1 );
sleep(1);
my $stat = $lxs->get;
my $pgswstats  = $stat->pgswstats;

print "pgpgin          $pgswstats->{pgpgin}\n";
print "pgpgout         $pgswstats->{pgpgout}\n";
print "pswpin          $pgswstats->{pswpin}\n";
print "pswpout         $pgswstats->{pswpout}\n";
print "pgfault         $pgswstats->{pgfault}\n";
print "pgmajfault      $pgswstats->{pgmajfault}\n";

#    pgpgin      -  Number of pages the system has paged in from disk per second.
#    pgpgout     -  Number of pages the system has paged out to disk per second.
#    pswpin      -  Number of pages the system has swapped in from disk per second.
#    pswpout     -  Number of pages the system has swapped out to disk per second.
#
#    The following statistics are only available by kernels from 2.6.
#
#    pgfault     -  Number of page faults the system has made per second (minor + major).
#    pgmajfault  -  Number of major faults per second the system required loading a memory page from disk.