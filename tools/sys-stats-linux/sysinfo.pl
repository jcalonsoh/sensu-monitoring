#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( sysinfo => 1 );
sleep(1);
my $stat = $lxs->get;
my $sysinfo  = $stat->sysinfo;

print "hostname      $sysinfo->{hostname}\n";
print "kernel        $sysinfo->{kernel}-$sysinfo->{release}\n";
print "memtotal      $sysinfo->{memtotal}\n";
print "swaptotal     $sysinfo->{swaptotal}\n";
print "uptime        $sysinfo->{uptime}\n";
print "idletime      $sysinfo->{idletime}\n";
print "interfaces    $sysinfo->{interfaces}\n";
print "countcpus     $sysinfo->{countcpus}\n";

#    hostname   -  The host name.
#    domain     -  The host domain name.
#    kernel     -  The kernel name.
#    release    -  The kernel release.
#    version    -  The kernel version.
#    memtotal   -  The total size of memory.
#    swaptotal  -  The total size of swap space.
#    uptime     -  The uptime of the system.
#    idletime   -  The idle time of the system.
#    pcpucount  -  The total number of physical CPUs.
#    tcpucount  -  The total number of CPUs (cores, hyper threading).
#    interfaces -  The interfaces of the system.
#    arch       -  The machine hardware name (uname -m).
#
#    # countcpus is the same like tcpucount
#    countcpus  -  The total (maybe logical) number of CPUs.