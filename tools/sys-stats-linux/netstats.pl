#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( netstats => 1 );
my $stat = $lxs->get;
my $netstats  = $stat->netstats;

print Dump decode_json to_json($netstats);

#    rxbyt    -  Number of bytes received per second.
#    rxpcks   -  Number of packets received per second.
#    rxerrs   -  Number of errors that happend while received packets per second.
#    rxdrop   -  Number of packets that were dropped per second.
#    rxfifo   -  Number of FIFO overruns that happend on received packets per second.
#    rxframe  -  Number of carrier errors that happend on received packets per second.
#    rxcompr  -  Number of compressed packets received per second.
#    rxmulti  -  Number of multicast packets received per second.
#    txbyt    -  Number of bytes transmitted per second.
#    txpcks   -  Number of packets transmitted per second.
#    txerrs   -  Number of errors that happend while transmitting packets per second.
#    txdrop   -  Number of packets that were dropped per second.
#    txfifo   -  Number of FIFO overruns that happend on transmitted packets per second.
#    txcolls  -  Number of collisions that were detected per second.
#    txcarr   -  Number of carrier errors that happend on transmitted packets per second.
#    txcompr  -  Number of compressed packets transmitted per second.
#    ttpcks   -  Number of total packets (received + transmitted) per second.
#    ttbyt    -  Number of total bytes (received + transmitted) per second.