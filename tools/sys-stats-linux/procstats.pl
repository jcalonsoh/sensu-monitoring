#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( procstats => 1 );
sleep(1);
my $stat = $lxs->get;
my $procstats  = $stat->procstats;

print "new          $procstats->{new}\n";
print "runqueue     $procstats->{runqueue}\n";
print "count        $procstats->{count}\n";
print "blocked      $procstats->{blocked}\n";
print "running      $procstats->{running}\n";

#    new       -  Number of new processes that were produced per second.
#    runqueue  -  The number of currently executing kernel scheduling entities (processes, threads).
#    count     -  The number of kernel scheduling entities that currently exist on the system (processes, threads).
#    blocked   -  Number of processes blocked waiting for I/O to complete (Linux 2.5.45 onwards).
#    running   -  Number of processes in runnable state (Linux 2.5.45 onwards).