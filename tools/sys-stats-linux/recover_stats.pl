#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;

my $lxs = Sys::Statistics::Linux->new(
    sysinfo => {
        init     => 1,
        initfile => './collected_stats/sysinfo.yml', #done -> metric
    },
    cpustats => {
        init     => 1,
        initfile => './collected_stats/cpustats.yml', #done -> metric metric & check
    },
    procstats => {
        init     => 1,
        initfile => './collected_stats/procstats.yml', #done -> metric
    },
    memstats => {
        init     => 1,
        initfile => './collected_stats/memstats.yml', #done -> metric metric & check
    },
    pgswstats => {
        init     => 1,
        initfile => './collected_stats/pgswstats.yml', #done -> metric
    },
    netstats => {
        init     => 1,
        initfile => './collected_stats/netstats.yml', #done -> metric
    },
    sockstats => {
        init     => 1,
        initfile => './collected_stats/sockstats.yml', #done -> metric
    },
    diskstats => {
        init     => 1,
        initfile => './collected_stats/diskstats.yml', #done -> metric & check
    },
    diskusage => {
        init     => 1,
        initfile => './collected_stats/diskusage.yml', #done -> metric
    },
    loadavg => {
        init     => 1,
        initfile => './collected_stats/loadavg.yml', #done -> metric & check
    },
    filestats => {
        init     => 1,
        initfile => './collected_stats/filestats.yml', #done -> metric
    },
    processes => {
        init     => 1,
        initfile => './collected_stats/processes.yml', #done -> special metric
    }
);

$lxs->get();

#    sysinfo     -  Collect system information              with Sys::Statistics::Linux::SysInfo.
#    cpustats    -  Collect cpu statistics                  with Sys::Statistics::Linux::CpuStats.
#    procstats   -  Collect process statistics              with Sys::Statistics::Linux::ProcStats.
#    memstats    -  Collect memory statistics               with Sys::Statistics::Linux::MemStats.
#    pgswstats   -  Collect paging and swapping statistics  with Sys::Statistics::Linux::PgSwStats.
#    netstats    -  Collect net statistics                  with Sys::Statistics::Linux::NetStats.
#    sockstats   -  Collect socket statistics               with Sys::Statistics::Linux::SockStats.
#    diskstats   -  Collect disk statistics                 with Sys::Statistics::Linux::DiskStats.
#    diskusage   -  Collect the disk usage                  with Sys::Statistics::Linux::DiskUsage.
#    loadavg     -  Collect the load average                with Sys::Statistics::Linux::LoadAVG.
#    filestats   -  Collect inode statistics                with Sys::Statistics::Linux::FileStats.
#    processes   -  Collect process statistics              with Sys::Statistics::Linux::Processes.