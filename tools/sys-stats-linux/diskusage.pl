#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( diskusage => 1 );
my $stat = $lxs->get;
my $diskusage  = $stat->diskusage;

print Dump decode_json to_json($diskusage);

#    major   -  The mayor number of the disk
#    minor   -  The minor number of the disk
#    rdreq   -  Number of read requests that were made to physical disk per second.
#    rdbyt   -  Number of bytes that were read from physical disk per second.
#    wrtreq  -  Number of write requests that were made to physical disk per second.
#    wrtbyt  -  Number of bytes that were written to physical disk per second.
#    ttreq   -  Total number of requests were made from/to physical disk per second.
#    ttbyt   -  Total number of bytes transmitted from/to physical disk per second.