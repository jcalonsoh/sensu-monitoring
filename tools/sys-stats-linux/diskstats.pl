#!/usr/bin/perl

use strict;
use warnings;
use Sys::Statistics::Linux;
use JSON;
use YAML::XS;

my $lxs = Sys::Statistics::Linux->new( diskstats => 1 );
sleep(1);
my $stat = $lxs->get;
my $diskstats  = $stat->diskstats;

print Dump decode_json to_json($diskstats);

#    total       -  The total size of the disk.
#    usage       -  The used disk space in kilobytes.
#    free        -  The free disk space in kilobytes.
#    usageper    -  The used disk space in percent.
#    mountpoint  -  The moint point of the disk.