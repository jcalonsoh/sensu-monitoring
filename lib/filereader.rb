def readfile(filename)
  begin
    return '' if filename == ''
    file_path = File.expand_path(filename)
    File.read(file_path)
  rescue => error
    puts "Faild to: #{error}"
  end
end